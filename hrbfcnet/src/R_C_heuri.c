
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "omp.h"
#include <R.h>
#include <Rinternals.h>


static float *voxel_med, *stdev;

//function to compute correlation
float correlation(float*voxel1, float*voxel2, int n, int k, int j) {

	float r = 0.0, xbar = 0.0, ybar = 0.0, sx = 0.0, sy = 0.0, sxy=0.0;
	int i;

	for(i = 0; i < n; ++i) {
		sxy += ((voxel1[i] - voxel_med[k])/stdev[k]) * ((voxel2[i] - voxel_med[j])/stdev[j]);
   	}
	sxy /= (n-1);  
	r = sxy;

	return r;

}

//function to compute the means and standard deviation
void calcula_media(float *voxel, int n, int j){
	int i;
	voxel_med[j] = 0;
	for(i=0; i<n; i++){
		voxel_med[j] += voxel[i];
	}

	voxel_med[j] /= n;

	for(i=0; i<n; i++){
		stdev[j] += (voxel[i] - voxel_med[j])* (voxel[i] - voxel_med[j]);
	
	}
	stdev[j] = sqrt((stdev[j] / (n-1)));
}
// heuristic: alloc = 307.2e9 * exp(-13.863x)

//main function
SEXP gera_rede(SEXP vox, SEXP th, SEXP num, SEXP tim) {

	char filename[50], fileout[50], number[10], time[10];
	int *aux_n, *aux_t, numb, P=0, t,i,j, **pairs, connections=1, total=0, allo=0, expon=0;
	float **voxels,c, *aux_thr;
	double *tmp_numb, *tmp_t, thr;
	SEXP rede;



	aux_n = INTEGER(num);
	aux_t = INTEGER(tim);
	tmp_t = REAL(th);

	numb = aux_n[0];
	t = aux_t[0];
	thr = tmp_t[0];
       

	expon = -13.862 * thr;
	allo = 307200000000 * exp(expon);

	
	PROTECT(rede = allocVector(REALSXP, allo)); P++;

	printf("voxels = %d \n timepoints = %d \n threshold = %f \n maximum estimated array size: %d\n",numb,t, thr, allo);
	
	
	printf("%d voxels in %d timepoints \n", numb, t);
	
	voxels = (float**) malloc(numb*sizeof(float*));
	puts("reading data\n");
	for(i=0; i<numb; i++){
		voxels[i] = (float*) malloc(t*sizeof(float));
		for(j=0; j<t; j++){
			voxels[i][j] = REAL(vox)[t * i + j]; 
		}
	}
	
	voxel_med = (float *) malloc(numb*sizeof(float));
	
	stdev = (float *) malloc(numb*sizeof(float));
	
	puts("computing means and standard deviations\n");
	for(i=0;i<numb;i++){
		calcula_media(voxels[i],t,i);
	}
	
	puts("computing the network\n");
	
	#pragma omp parallel private(i,j,c)
	{
		#pragma omp for schedule(dynamic) reduction(+:connections)
		for(i=0;i<1000;i++){
			for(j=i+1;j<numb;j++){
				c = correlation(voxels[i],voxels[j],t,i,j);
				if(c >= thr){
					connections++;
					#pragma omp critical
					{
					REAL(rede)[total] = i+1;
					REAL(rede)[total+1] = j+1;
					total +=2;
					}
				}
			}
			printf("%d\n",i);
		}
		
		
		#pragma omp single
		{
			printf("number of connections = %d\n",connections);
		}
		
	}	
	UNPROTECT(P);

	return rede;

}



