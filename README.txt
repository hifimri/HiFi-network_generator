---------------------------------------------------
High Resolution BFC network generator - version 1.0
---------------------------------------------------

Compilation:
>R CMD SHLIB <file.c>

Usage:
inside R shell:

source("generate_BFC.R")
edge_list <- generate_BFC("<datapath>",<correlation_threshold>)


example:

el <- generate_BFC("/home/user/mydata/subject3.nii",0.5)
